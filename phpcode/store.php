<?php

if (isset($_SERVER['HTTP_ORIGIN'])) {
    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
    // you want to allow, and if so:
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
    // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}





include 'db.php';

//put into secure place
//include '../../db.php';

$postdata = file_get_contents('php://input');

if (isset($postdata) && !empty($postdata)) {
    $request = json_decode($postdata);


    $traveller = mysqli_real_escape_string($conn, $request->traveller);
    $title = mysqli_real_escape_string($conn, $request->title);
    $spot = mysqli_real_escape_string($conn, $request->spot);
    $note = mysqli_real_escape_string($conn, $request->note);
    $image = mysqli_real_escape_string($conn, $request->image);
    $lat = mysqli_real_escape_string($conn, $request->lat);
    $lng = mysqli_real_escape_string($conn, $request->lng);
    $mobileuserid = mysqli_real_escape_string($conn, $request->mobileuserid);
    $date = mysqli_real_escape_string($conn, $request->date);
    
    $traveller = mb_convert_encoding($traveller, "ISO-8859-2", "UTF-8");
    $title = mb_convert_encoding($title, "ISO-8859-2", "UTF-8");
    $spot = mb_convert_encoding($spot, "ISO-8859-2", "UTF-8");
    $note = mb_convert_encoding($note, "ISO-8859-2", "UTF-8");
    $mobileuserid = mb_convert_encoding($mobileuserid, "ISO-8859-2", "UTF-8");
    //echo " | mobileuserid: ".$mobileuserid. " | ";
    $creation_date = $date;


    $sql = "SELECT id  FROM traveldatapost_posts WHERE image = '$image'  ORDER BY upload_date DESC LIMIT 1";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    //echo " | info: ".$sql  . " | ";
    //var_dump($row["id"]);

    if (empty($row["id"])) {
        $sql = "INSERT INTO traveldatapost_posts(traveller, title, spot, note, image, lat, lng, mobileuserid,  creation_date, upload_date) "
                . " VALUES ('$traveller','$title', '$spot', '$note', '$image', '$lat', '$lng', '$mobileuserid',  '$creation_date', NOW()) ";
        $result = mysqli_query($conn, $sql);

        if ($result) {
            echo "success";
      //  echo "sql : " . $sql;
        } else {
            echo "failed: " . $sql;
        }
    } else {


        $sql = "UPDATE traveldatapost_posts SET traveller = '$traveller' , title = '$title', "
                . "spot = '$spot' , note = '$note' , image = '$image' , lat = '$lat', lng = '$lng' , "
                . "mobileuserid ='$mobileuserid' , creation_date = '$creation_date'  WHERE id=" . $row["id"];
        
        $result = mysqli_query($conn, $sql);
        

        if ($result) {
            echo "success";
      //    echo "sql : " . $sql;
        } else {
            echo "failed: " . $sql;
        }
    }
}
?>