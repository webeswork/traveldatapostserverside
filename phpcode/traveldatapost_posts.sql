
CREATE TABLE `traveldatapost_posts` (
  `id` int(11) NOT NULL,
  `traveller` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `spot` varchar(255) NOT NULL,
  `note` varchar(500) NOT NULL,
  `image` varchar(255) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `mobileuserid` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL,
  `upload_date` datetime NOT NULL
) CHARSET=utf8;


