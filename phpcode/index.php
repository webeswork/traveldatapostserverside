<?php
header('Content-Type: text/html; charset=utf-8');
//list data

include 'db.php';

//put it into secure place
//include '../../db.php';


    $sql = "SELECT *  FROM traveldatapost_posts   ORDER BY creation_date DESC";
    $result = mysqli_query($conn, $sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Travel Data Post example data</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<style> table {
border-collapse: collapse;
}

table, th, td {
border: 1px solid #ccc;
padding: 10px;
}
</style>
</head>
<body>

  
<div class="container">
<h2>Travel Data Post example data</h2>
  
<table>
<tr>
<th>Picture</th>
<th>Title</th>
<th> Traveller</th>
<th>Location</th>
<th>Description</th>
<th>Position(Lat, Lng)</th>
<th>Created</th>
</tr>
<?php while ($row = mysqli_fetch_assoc($result)){ ?>
<tr>
<td><img src="../uploads/<?php echo $row['image']; ?>" style="height:300px;"/></td>
<td><?php echo $row['title']; ?></td>
<td><?php echo $row['traveller']; ?></td>
<td><?php echo $row['spot']; ?></td>
<td><?php echo $row['note']; ?></td>
<td><a href="https://www.google.com/maps/?q=<?php echo $row['lat']; ?>,<?php echo $row['lng']; ?>" target="_blank"><?php echo $row['lat']; ?>,<?php echo $row['lng']; ?> position</a></td>
<td> <?php echo $row['creation_date']; ?></td>
</tr>
<?php } ?>
</table>

</div>

</body>
</html>
